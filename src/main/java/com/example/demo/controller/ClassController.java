package com.example.demo.controller;

import com.example.demo.domain.AptechClass;
import com.example.demo.domain.Student;
import com.example.demo.service.AptechClassService;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "/admin/class")
public class ClassController {
    @Autowired
    AptechClassService aptechClassService;

    @Autowired
    StudentService studentService;

    @RequestMapping(method = RequestMethod.GET, value = "/store")
    public String store(@RequestParam(name = "id") String id, @RequestParam(name = "studentId") String studentId) {
        int clazzId = Integer.parseInt(id);
        Student st = new Student();
        AptechClass aptechClass = aptechClassService.getDetail(clazzId);
        String[] strings = studentId.split(",");
        for (String stId : strings) {
            st = studentService.findById(Integer.parseInt(stId));
            aptechClass.setStudents(st);
        }
        aptechClassService.store(aptechClass);
        return "redirect:/admin/class";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", aptechClassService.getList(1, 10));
        return "/clazz/list";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public String getDetail(@PathVariable int id, Model model) {
        model.addAttribute("class", aptechClassService.getDetail(id));
        model.addAttribute("listStudent", studentService.getList(1, 10));
        return "/clazz/detail";
    }
}
