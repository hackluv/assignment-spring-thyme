package com.example.demo.repository;

import com.example.demo.domain.AptechClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AptechClassRepository extends JpaRepository<AptechClass, Integer> {
}
