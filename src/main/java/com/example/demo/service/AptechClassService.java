package com.example.demo.service;

import com.example.demo.domain.AptechClass;
import com.example.demo.repository.AptechClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class AptechClassService {
    @Autowired
    AptechClassRepository aptechClassRepository;

    public Page<AptechClass> getList(int page, int limit) {
        return aptechClassRepository.findAll(PageRequest.of(page - 1, limit));
    }

    public AptechClass getDetail(int id) {
        return aptechClassRepository.findById(id).orElse(null);
    }

    public AptechClass store(AptechClass aptechClass){
        return aptechClassRepository.save(aptechClass);
    }
}
