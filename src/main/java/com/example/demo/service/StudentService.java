package com.example.demo.service;

import com.example.demo.domain.AptechClass;
import com.example.demo.domain.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public Page<Student> getList(int page, int limit) {
        return studentRepository.findAll(PageRequest.of(page - 1, limit));
    }

    public Student register(Student student){
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        return studentRepository.save(student);
    }

    public Student login(String email, String passsword){
        Optional<Student> optionalStudent = studentRepository.findByEmail(email);
        if (optionalStudent.isPresent()){
            Student student = optionalStudent.get();
            if (student.getPassword().equals(passwordEncoder.encode(passsword))){
                return student;
            }
        }
        return null;
    }

    public Student findByEmail(String email){
        Optional<Student> optionalStudent = studentRepository.findByEmail(email);
        return optionalStudent.orElse(null);
    }

    public Student findById(int studentId) {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        return optionalStudent.orElse(null);
    }
}
